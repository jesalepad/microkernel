# <<Microkernel>> #

## Project for CS 452/642 course. 

Implementation of a complete [microkernel](http://en.wikipedia.org/wiki/Microkernel). It contains the following functionality:

   * Task management

   * Memory management

   *  Scheduling

   *  Context switching

   *  I/O Control

   *  Train test application (runs on top of the microkernel)

   *  Etc.

The project was developed by a team of three:

- jesalepad (jesalepad@gmail.com)

- rpavlovs

- pvalov